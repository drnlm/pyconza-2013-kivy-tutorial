from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.app import App
from kivy.lang import Builder

Builder.load_string("""
<TouchHolder>:
    motion: motion
    touch: touch
    Label:
        id: motion
        text: 'Touch move'
    Label:
        id: touch
        text: 'Touch down'
""")


class TouchHolder(Widget):

    def __init__(self):
        super(TouchHolder, self).__init__()
        self.motion.bind(on_touch_down=self.touch_down)
        self.touch.bind(on_touch_move=self.touch_move)

    def touch_down(self, instance, touch):
        if 'button' in touch.profile:
            self.touch.text = 'Touch %s down at %d, %d (button %s)' % (
                touch.id, int(touch.x), int(touch.y), touch.button)
        else:
            self.touch.text = 'Touch %d down at %d, %d' % (
                touch.id, int(touch.x), int(touch.y))

    def touch_move(self, instance, touch):
        if 'button' in touch.profile:
            self.motion.text = 'Touch %s motion at %d, %d (button %s)' % (
                touch.id, int(touch.x), int(touch.y), touch.button)
        else:
            self.motion.text = 'Touch %s motion at %d, %d' % (
                touch.id, int(touch.x), int(touch.y))


class EventApp(App):

    def build(self):
        return TouchHolder()


if __name__ == "__main__":
    EventApp().run()
