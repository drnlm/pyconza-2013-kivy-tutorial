from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.app import App
from kivy.lang import Builder

Builder.load_string("""
<TouchHolder>:
    motion: motion
    touch: touch
    Label:
        id: motion
        pos: 200, 200
        text: 'Touch move'
    Label:
        id: touch
        pos: 200, 400
        text: 'Touch down'
""")


class TouchHolder(Widget):

    def on_touch_down(self, touch):
        if 'button' in touch.profile:
            self.touch.text = 'Touch %s down at %d, %d (button %s)' % (
                touch.id, int(touch.x), int(touch.y), touch.button)
        else:
            self.touch.text = 'Touch %d down at %d, %d' % (
                touch.id, int(touch.x), int(touch.y))

    def on_touch_move(self, touch):
        if 'button' in touch.profile:
            self.motion.text = 'Touch %s motion at %d, %d (button %s)' % (
                touch.id, int(touch.x), int(touch.y), touch.button)
        else:
            self.motion.text = 'Touch %s motion at %d, %d' % (
                touch.id, int(touch.x), int(touch.y))



class EventApp(App):

    def build(self):
        return TouchHolder()


if __name__ == "__main__":
    EventApp().run()
