from kivy.uix.label import Label
from kivy.app import App


class HelloApp(App):

    def build(self):
        return Label(text='Hello World', font_size=30)


if __name__ == "__main__":
    HelloApp().run()
