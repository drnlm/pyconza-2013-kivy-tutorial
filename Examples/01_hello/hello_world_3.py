from kivy.app import App
from kivy.clock import Clock
from kivy.properties import NumericProperty
from kivy.uix.label import Label

class MyLabel(Label):

    angle = NumericProperty()

    def update(self, dt):
        self.angle += 5
        if self.angle >= 360:
            self.angle = 0


class Hello3App(App):

    def build(self):
        widget = MyLabel()
        Clock.schedule_interval(widget.update, 0.1)
        return widget


if __name__ == "__main__":
    Hello3App().run()
