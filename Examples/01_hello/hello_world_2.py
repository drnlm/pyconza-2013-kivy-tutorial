from kivy.uix.label import Label
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics import Line, Color, Rectangle

class MyWidget(Label):

    def __init__(self, **kwargs):
        super(MyWidget, self).__init__(**kwargs)
        self._start_angle = 0
        self._inc = 5

    def update(self, dt):
        self._start_angle += self._inc
        if self._start_angle == 360 or self._start_angle == 0:
            self._inc = -self._inc
        self.canvas.clear()
        with self.canvas:
            Color(1, 1, 0)
            Line(ellipse=(200, 200, 150, 100, self._start_angle, 360))
            Rectangle(texture=self.texture, pos=(225, 225), size=(75, 50))


class HelloApp(App):

    def build(self):
        widget = MyWidget(text='Hello World')
        Clock.schedule_interval(widget.update, 0.1)
        return widget


if __name__ == "__main__":
    HelloApp().run()
