from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.app import App
from kivy.lang import Builder

Builder.load_string("""
<TouchHolder>:
    motion: motion
    touch: touch
    size: 300, 300
    Label:
        id: motion
        pos_hint: {'center_x': 0.5, 'y': 0.3}
        size_hint: 1, 0.2
        text: 'Touch move'
    Label:
        id: touch
        pos_hint: {'center_x': 0.5, 'y': 0.7}
        size_hint: 1, 0.2
        text: 'Touch down'
""")


class TouchHolder(FloatLayout):

    def on_touch_down(self, touch):
        if 'button' in touch.profile:
            self.touch.text = 'Touch %s down at %d, %d (button %s)' % (
                touch.id, int(touch.x), int(touch.y), touch.button)
        else:
            self.touch.text = 'Touch %d down at %d, %d' % (
                touch.id, int(touch.x), int(touch.y))

    def on_touch_move(self, touch):
        if 'button' in touch.profile:
            self.motion.text = 'Touch %s motion at %d, %d (button %s)' % (
                touch.id, int(touch.x), int(touch.y), touch.button)
        else:
            self.motion.text = 'Touch %s motion at %d, %d' % (
                touch.id, int(touch.x), int(touch.y))



class EventApp(App):

    def build(self):
        widget = TouchHolder()
        return widget


if __name__ == "__main__":
    EventApp().run()
